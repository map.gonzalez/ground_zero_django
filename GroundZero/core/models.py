from django.db import models

# Create your models here.

opciones_consultas =[
[0, "Consulta"],
[1, "Reclamo"],
[2, "Sugerencia"],
[3, "Felicitaciones"]
]

class Contacto (models.Model):
    nombre = models.CharField(max_length=50)
    correo = models.EmailField()
    tipo_consulta = models.IntegerField(choices=opciones_consultas)
    mensaje = models.TextField()
    avisos = models.BooleanField()

    def __str__(self):
        return self.nombre

class Artista(models.Model):
    nombre = models.CharField(max_length=50)
    
    def __str__(self):
        return self.nombre


class Categoria(models.Model):
    nombre = models.CharField(max_length=50)
    
    def __str__(self):
        return self.nombre


class Producto(models.Model):
    nombre = models.CharField(max_length=50)
    precio = models.IntegerField()
    categoria = models.ForeignKey(Categoria, on_delete=models.PROTECT)
    descripcion = models.TextField()
    artista = models.ForeignKey(Artista, on_delete=models.PROTECT)
    nuevo = models.BooleanField()
    imagen = models.ImageField(upload_to="productos", null=True)

    def __str__(self):
        return self.nombre
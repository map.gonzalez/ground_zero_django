from django.shortcuts import render, redirect, get_object_or_404
from .models import Producto
from .forms import ContactoFormulario, ProductoFormulario, CustomUserCreationForm
from django.contrib import messages
from django.core.paginator import Paginator
from django.http import Http404
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required, permission_required

# Create your views here.

def home(request): 
        return render(request,'core/home.html')

def listado(request): 
        productos = Producto.objects.all()
        data ={
                'productos': productos
        }
        return render(request,'core/listado.html',data)

def pintura(request): 
        return render(request,'core/pintura.html')

def escultura(request): 
        return render(request,'core/escultura.html')

def tejido(request): 
        return render(request,'core/tejido.html')

def orfebreria(request): 
        return render(request,'core/orfebreria.html')

def otras(request): 
        return render(request,'core/otras.html')

def quienes(request): 
        return render(request,'core/quienes-somos.html')

def contacto(request): 
        data ={
                'form': ContactoFormulario()
        }
        if request.method == 'POST':
                formulario = ContactoFormulario(data=request.POST)
                if formulario.is_valid():
                        formulario.save()
                        messages.success(request, "Mensaje enviado")
                else: 
                        data ["form"] = formulario
        return render(request,'core/contacto.html',data)

@permission_required('core.add_producto')
def agregar_producto(request):
        data ={
                'form': ProductoFormulario()
        }
        if request.method == 'POST':
                formulario = ProductoFormulario(data=request.POST, files=request.FILES)
                if formulario.is_valid():
                        formulario.save()
                        messages.success(request, "Producto registrado")
                else: 
                        data ["form"] = formulario
        return render(request,'core/producto/agregar.html',data)

@permission_required('core.view_producto')
def listar_productos(request): 
        productos = Producto.objects.all()
        page = request.GET.get('page',1)

        try: 
                paginator = Paginator(productos, 5)
                productos = paginator.page(page)
        except:
                raise Http404


        data = {
                'productos': productos,
                'paginator': paginator
        }

        return render(request,'core/producto/listar.html',data)

@permission_required('core.change_producto')
def editar_producto(request, id): 
        
        producto = get_object_or_404(Producto, id=id)

        data = {
                'form': ProductoFormulario(instance=producto)
        }
        
        if request.method == 'POST':
                formulario = ProductoFormulario(data=request.POST, instance=producto, files=request.FILES)
                if formulario.is_valid():
                        formulario.save()
                        messages.success(request, "Producto editado correctamente")
                        return redirect(to="listar_productos")
                data["form"] = formulario

        return render(request,'core/producto/editar.html', data)

@permission_required('core.delete_producto')
def eliminar_producto(request, id): 
        producto = get_object_or_404(Producto, id=id)
        producto.delete()
        messages.success(request, "Producto eliminado correctamente")
        return redirect(to="listar_productos")     


def registro(request): 
        data = {
                'form': CustomUserCreationForm()
        }

        if request.method == 'POST':
                formulario = CustomUserCreationForm(data=request.POST)
                if formulario.is_valid():
                        formulario.save()
                        user = authenticate(username=formulario.cleaned_data["username"], password=formulario.cleaned_data["password1"])
                        login(request, user)
                        messages.success(request, "Registrado correctamente")
                        return redirect(to="home")
                data["form"] = formulario
        
        return render(request,'registration/registro.html',data)
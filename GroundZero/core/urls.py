from django.urls import path
from .views import home, pintura, escultura, tejido, orfebreria, otras, quienes, contacto, listado, agregar_producto, listar_productos, editar_producto, eliminar_producto, registro

urlpatterns = [
    path('', home, name="home"),
    path('pintura/', pintura, name="pintura"),
    path('escultura/', escultura, name="escultura"),
    path('tejido/', tejido, name="tejido"),
    path('orfebreria/', orfebreria, name="orfebreria"),
    path('otras/', otras, name="otras"),
    path('quienes/', quienes, name="quienes"),
    path('contacto/', contacto, name="contacto"),
    path('listado/', listado, name="listado"),
    path('agregar/', agregar_producto, name="agregar_producto"),
    path('listar/', listar_productos, name="listar_productos"),
    path('editar/<id>/', editar_producto, name="editar_producto"),
    path('eliminar/<id>/', eliminar_producto, name="eliminar_producto"),
    path('registro/', registro, name="registro"),
]

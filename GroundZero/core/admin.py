from django.contrib import admin
from .models import Contacto, Artista, Producto, Categoria

# Register your models here.
class ProductoAdmin(admin.ModelAdmin):
    list_display = ["nombre", "precio", "artista", "nuevo"]
    list_editable = ["precio"]
    search_fields = ["nombre"]
    list_filter = ["artista","nuevo", "categoria"]
    list_per_page = 10

admin.site.register(Contacto)
admin.site.register(Artista)
admin.site.register(Categoria)
admin.site.register(Producto, ProductoAdmin)

